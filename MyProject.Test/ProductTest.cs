﻿namespace MyProject.Test
{
    using System.Collections.Generic;
    using System.Net.Configuration;
    using Model;
    using Moq;
    using Ninject;
    using Ninject.MockingKernel.Moq;
    using NUnit.Framework;
    using Service;

    [TestFixture]
    public class ProductTest
    {
        private readonly MoqMockingKernel _kernel;

        private IProductService _productService;

        private Mock<IProductRepository> _productRepositoryMock;
        private Mock<ICategoryRepository> _categoryRepositoryMock;

        public ProductTest()
        {
            _kernel = new MoqMockingKernel();
            _kernel.Bind<IProductService>().To<ProductService>();
        }

        [SetUp]
        public void Setup()
        {
            _productService = _kernel.Get<IProductService>();
            _productRepositoryMock = _kernel.GetMock<IProductRepository>();
            _categoryRepositoryMock = _kernel.GetMock<ICategoryRepository>();
        }

        [Test]
        public void GetProductListTest()
        {
            var category = new Category {Id = 1, Name = "Test"};
            var products = new List<Product>()
            {
                new Product {Id = 1, Name = "Test1", IsActive = true, Category = category},
                new Product {Id = 2, Name = "Test 2", IsActive = false, Category = category}
            };
            _productRepositoryMock.Setup(mock => mock.GetProducts(x => x.Name == category.Name && x.IsActive))
                .Returns(products);
            _productService.GetProducts(category);
            _productRepositoryMock.VerifyAll();
        }
    }
}
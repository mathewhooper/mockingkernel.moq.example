﻿namespace MyProject.Service
{
    using System.Collections.Generic;
    using Model;

    public interface IProductService
    {
        Product GetProduct();
        List<Product> GetProducts(Category category);
    }

    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Product GetProduct()
        {
            return _productRepository.GetProduct(1);
        }

        public List<Product> GetProducts(Category category)
        {
            return _productRepository.GetProducts(x => x.Name == category.Name && x.IsActive);
        }
    }
}
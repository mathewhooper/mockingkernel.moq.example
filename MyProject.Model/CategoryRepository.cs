﻿namespace MyProject.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public interface ICategoryRepository
    {
        Category GetProduct(int id);
        List<Category> GetProducts(Expression<Func<Category, Boolean>> where);

    }

    public class CategoryRepository : ICategoryRepository
    {
        public Category GetProduct(int id)
        {
            return new Category
            {
                Id = 1,
                Name = "Cat 1"
            };
        }

        public List<Category> GetProducts(Expression<Func<Category, bool>> @where)
        {
            return new List<Category>()
            {
                new Category { Id = 1, Name = "Category 1"},
                new Category { Id = 2, Name = "Category 2"}
            };
        }
    }
}
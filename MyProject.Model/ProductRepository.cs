﻿namespace MyProject.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public interface IProductRepository
    {
        Product GetProduct(int id);
        List<Product> GetProducts(Expression<Func<Product, Boolean>> where);

    }

    public class ProductRepository : IProductRepository
    {
        public Product GetProduct(int id)
        {
            var category = new Category
            {
                Id = 1,
                Name = "Cat 1"
            };
            return new Product()
            {
                Id = 1,
                Name = "Test",
                IsActive = true,
                Category = category
            };
        }

        public List<Product> GetProducts(Expression<Func<Product, bool>> where)
        {
            var category = new Category
            {
                Id = 1,
                Name = "Cat 1"
            };
            return new List<Product>()
            {
                new Product
                {
                    Id = 1,
                    Name = "Test",
                    IsActive = true,
                    Category = category
                },
                new Product
                {
                    Id = 2,
                    Name = "Test2",
                    IsActive = false,
                    Category = category
                }
            };
        }
    }
}